# icons

## Library

### 1. Logo
- [main](https://gitlab.com/openintacht/gaussian-curv/ui-ux/icons/-/blob/master/logo/main.svg): by [@mathegic100](https://gitlab.com/mathegic100) & [@UTx10101](https://gitlab.com/UTx10101)

## Maintainers
- Aritra Mondal - [@mathegic100](https://gitlab.com/mathegic100)
- Utkrisht Singh Chauhan - [@UTx10101](https://gitlab.com/UTx10101)

## Contributors

No buddy here yet.. :(